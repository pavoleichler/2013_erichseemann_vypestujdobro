<?php

namespace Models;

abstract class ExtendedJoinedTable extends JoinedTable
{
    
    
    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * 
     *                             Model behaviour
     * 
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
    
    /**
     * Extends the given fluent select with additional data rom joined tables.
     * You can create a complex join, which will retrieve several rows for each object. Then you would parse the results in fetch method, using the dibi::fetchAssoc().
     * As extend is alwyas called only after the limit, offset and order parameters have been applied, it will not interfere with paging.
     * 
     * @param \DibiFluent $fluent
     * @return \DibiFluent
     */
    protected function extend(\DibiFluent $fluent) {
        
        return $this->select()->from($fluent, key($this->table));
        
    }
    
    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * 
     *                             Selects
     * 
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
    
    /**
     * Returns the extended basic select.
     * Call withou arguments to retrieve the default fields, or pass custom fields in the same way, you would call the dibi::select() method.
     * 
     * @return \DibiFluent
     */
    protected function extended() {
        
        $args = func_get_arg();
        $select = call_user_func_array(array($this, 'select'), $args);
        
        return $this->extend($select);
        
    }
    
    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * 
     *                                 Tools
     * 
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
    
    protected function fetchAll(\DibiFluent $fluent) {
        
        $this->extend($fluent);
        
        return parent::fetchAll($fluent);
    }
    
    protected function fetchOne(\DibiFluent $fluent) {
        
        $this->extend($fluent);
        
        return parent::fetchOne($fluent);
    }

}
<?php

namespace Models;

use Nette\Security\Permission;

class Authorizator extends Permission {

    public function __construct($roles = array(), $resources = array(), $permissions = array()) {

        // roles
        if (is_array($roles)) {
            foreach ($roles as $name => $parent)
                $this->addRole($name, $parent);
        }

        // resources
        if (is_array($resources)) {
            foreach ($resources as $name => $parent)
                $this->addResource($name, $parent);
        }

        // permissions
        if (is_array($permissions)) {
            foreach ($permissions as $role => $resources) {
                // no privileges for this role defined
                if (!is_array($resources))
                    continue;
                foreach ($resources as $resource => $privileges) {
                    $privileges = $this->parsePrivileges($privileges);
                    $this->allow($role, $resource, $privileges);
                }
            }
        }
        
    }
    
    /**
     * Parses the privilleges definition.
     * 
     * @param $definition
     * @return mixed
     */
    protected function parsePrivileges($definition) {

        if (trim($definition) == 'all' || trim($definition) == '*')
            return Permission::ALL;
        
        if (is_string($definition))
            $privileges = explode (',', $definition);
        
        return $privileges;
        
    }

}

?>
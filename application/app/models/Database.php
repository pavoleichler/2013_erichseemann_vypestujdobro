<?php

namespace Models;

abstract class Database extends \Nette\Object
{

    /** @var \DibiConnection */
    protected $dibi;

    /**
     * 
     * @param \DibiConnection $dibi
     */
    public function __construct(\DibiConnection $dibi){
        
        $this->dibi = $dibi;
        
    }

    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * 
     *                               Tools
     * 
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
    
    /**
     * Filters out all array keys not stated as values in the $columns array from the $values array.
     * Example:
     * $values = array('a' => 1, 'b' => 2, 'c' => 3);
     * $columns = array('a', 'c');
     * $this->filterValues($values, $columns); // returns array('a' => 1, 'c' => 3)
     * 
     * @param array $values The original array to apply the filter on.
     * @param array $columns Filter mask, an array of allowed keys for the $values array.
     * @return array Filtered array.
     */
    protected function filterValues($values, $columns) {
        
        if ($columns)
            return array_intersect_key($values, array_flip($columns));
        else
            return $columns;
        
    }

}
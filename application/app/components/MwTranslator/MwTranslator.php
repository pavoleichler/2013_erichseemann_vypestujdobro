<?php

/**
 * @author malware 
 */

namespace Mw\Translator;

use Nette\Utils\Json,
    \Nette\Caching\Cache;

class MwTranslator extends \Nette\Application\UI\Control implements \Nette\Localization\ITranslator
{

    /** @var Cache */
    private $cache;

    /** @var string */
    private $cacheKey = 'structure';

    /** @var array */
    private $sourceArray;

    /** @var string */
    private $defaultSection = 'default';
    
    /** @var string */
    private $section;

    /** @var string */
    private $filepath;

    /** @var object */
    private static $instance;
    
    /** 
     * Hodnota posielana z handleru do metody render
     * @param string
     */
    protected $sendedSection;
    
    /**
     * Provides access to translated strings of the project.
     * 
     * @param type $section Unique string identifying the language.
     */
    public function __construct(Cache $cache, $structureFile, $section = NULL)
    {
        $this->section = $section ? : $this->defaultSection;
        $this->filepath = $structureFile;
        $this->cache = $cache;

        if (!$this->sourceArray)
            $this->sourceArray = $this->getSource();
    }
    
    /**
     * Set language to use.
     * 
     * @param type $section Unique string identifying the language.
     */
    public function setSection($section) {
        
        $this->section = $section;
        
    }

    /**
     * Get and parse the raw data.
     * 
     * @return array Parsed source data. 
     */
    private function getSource()
    {
        $cacheKey = $this->cacheKey;
        if ($this->cache[$cacheKey])
            return $this->cache[$cacheKey];

        if (!file_exists($this->filepath))
            file_put_contents('safe://' . $this->filepath, $this->getBaseJsonSource());

        $fileContent = file_get_contents('safe://' . $this->filepath);
        $source = Json::decode($fileContent, Json::FORCE_ARRAY);
        $this->cache->save($cacheKey, $source, array(Cache::FILES => $this->filepath));

        return $source;
    }

    /**
     * Get an empty data source array.
     * 
     * @return array Empty source data.
     */
    private function getBaseJsonSource()
    {
        return Json::encode(array());
    }

    /**
     * Translate a given key to the language defined in $section or in the current section if omitted.
     * 
     * @param string $message Message unique key.
     * @param string $section Unique language identifier.
     * @param mixed $sprintfArgs String or an array of strings to substitute the printf macros with.
     * @return string Translated message.
     */
    public function translate($message, $section = NULL, $sprintfArgs = NULL)
    {
        $result = $this->find($message, $section);
        
        if ($sprintfArgs && is_array($sprintfArgs))
            return vsprintf($result, $sprintfArgs);
        elseif ( $sprintfArgs && is_scalar($sprintfArgs) )
            return sprintf($result, $sprintfArgs);
        else
            return $result;    
    }

    /**
     * Looks up the given key int the defined $section.
     * 
     * @param string $message Message unique key.
     * @param string $section Unique language identifier.
     * @return type Translated message.
     */
    private function find($message, $section)
    {   
        $section = $section ? : $this->section;
        $message = \Nette\Utils\Strings::normalize($message);

        if (!$message)
            return false;
        
        $writeNewSource = FALSE;

        if (!isset($this->sourceArray[$section]))
            $this->sourceArray[$section] = array();

        if (!array_key_exists($message, $this->sourceArray[$section]))
        {
            $this->sourceArray[$section][$message] = $message;
            $writeNewSource = TRUE;
        }

        if ($writeNewSource)
            $this->save();

        return $this->sourceArray[$section][$message];
    }

    /**
     * Save the current source data to file.
     */
    private function save()
    {
        file_put_contents('safe://' . $this->filepath, $this->jsonIndent(Json::encode($this->sourceArray)));
        $this->clearSourceCache();
    }

    /**
     * Static method to translate the given message to a defined language.
     * 
     * @param string $message Message unique key.
     * @param string $section Unique language identifier.
     * @param mixed $sprintfArgs String or an array of strings to substitute the printf macros with.
     * @return string Translated message.
     */
    public static function get($message, $section = NULL, $sprintfArgs = NULL)
    {
        if (!self::$instance)
            $self = self::$instance = new static;
        else 
            $self = self::$instance;
        
        $result = $self->find($message, $section ? : $self->defaultSection);
        
        if ($sprintfArgs && is_array($sprintfArgs))
            return vsprintf($result, $sprintfArgs);
        elseif ( $sprintfArgs && is_scalar($sprintfArgs) )
            return sprintf($result, $sprintfArgs);
        else
            return $result;        
    }

    /**
     * Format a provided inline JSON string to a human readable form.
     * 
     * @param string $json JSON string.
     * @return string Human readable JSON string.
     */
    private function jsonIndent($json)
    {
        $result = '';
        $pos = 0;
        $strLen = strlen($json);
        $indentStr = '  ';
        $newLine = "\n";
        $prevChar = '';
        $outOfQuotes = true;

        for ($i = 0; $i <= $strLen; $i++)
        {
            $char = substr($json, $i, 1);

            if ($char == '"' && $prevChar != '\\')
            {
                $outOfQuotes = !$outOfQuotes;
            }
            else if (($char == '}' || $char == ']') && $outOfQuotes)
            {
                $result .= $newLine;
                $pos--;
                for ($j = 0; $j < $pos; $j++)
                {
                    $result .= $indentStr;
                }
            }

            $result .= $char;

            if (($char == ',' || $char == '{' || $char == '[') && $outOfQuotes)
            {
                $result .= $newLine;
                if ($char == '{' || $char == '[')
                {
                    $pos++;
                }

                for ($j = 0; $j < $pos; $j++)
                {
                    $result .= $indentStr;
                }
            }

            $prevChar = $char;
        }

        return $result;
    }

    /**
     * Remove source cache.
     */
    private function clearSourceCache()
    {
        unset($this->cache[$this->cacheKey]);
    }

    /**
     * Handle an HTTP call to modify source data. Used by the admin interface.
     */
    public function handleSaveSource()
    {
        $params = $this->presenter->params;
        $value  = $message = \Nette\Utils\Strings::normalize($params['value']);

        $this->sendedSection = $params['section'];
        
        $this->sourceArray[$params['section']][$params['key']] = $value;
        $this->save();

        if ($this->presenter->isAjax())
            $this->invalidateControl('mw_translator');
        else
            $this->presenter->redirect('this');
    }

    /**
     * Handle an HTTP call to remove an entry from source data. Used by the admin interface.
     */
    public function handleRemoveSource()
    {
        $params = $this->presenter->params;

        if (isset($this->sourceArray[$params['section']][$params['key']]))
            unset($this->sourceArray[$params['section']][$params['key']]);
        
        $this->sendedSection = $params['section'];
        
        $this->save();

        if ($this->presenter->isAjax())
            $this->invalidateControl('mw_translator');
        else
            $this->presenter->redirect('this');
    }

    /**
     * Display the translator administration interface.
     */
    public function render()
    {
        $params = $this->presenter->request->parameters;
        
        $this->template->sourceNames = array_keys($this->sourceArray);

        if ($this->sendedSection)
            $this->template->actualSectionName = $this->sendedSection;
        elseif (isset($params['mw-section']))
            $this->template->actualSectionName = $params['mw-section'];
        else
            $this->template->actualSectionName = isset($this->template->sourceNames[0]) ?  $this->template->sourceNames[0] : null;

        
        $this->template->source = $this->sourceArray;
        $this->template->setFile(__DIR__ . '/translator.latte');
        $this->template->render();
    }
}
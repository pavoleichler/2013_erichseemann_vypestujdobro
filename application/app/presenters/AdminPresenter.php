<?php

/**
 * Admin presenter.
 *
 * @author     Pavol Eichler
 */
class AdminPresenter extends BasePresenter
{
    
    protected function startup() {
        parent::startup();
        
        $this->template->title = 'Administrácia';
        
        $this->template->meta = array();
        
        $this->template->facebook = array(
            'appId' => $this->context->params['facebook']['appId'],
            'locale' => $this->context->params['facebook']['locale']
        );
        
        $this->context->httpAuth->protect();
        
    }

    protected function createComponentTranslator() {
        
        return $this->context->translator;
        
    }

}
<?php

/**
 * Description of Base
 *
 * @author gabo
 */

namespace Models;

use Nette\Caching\Cache;

abstract class Base extends \Nette\Object
{

    /** @var \dibi */
    protected $dibi;
    protected $cache;

    public function __construct($dibi, $cache)
    {
        $this->dibi = $dibi;
        $this->cache = $cache;
    }


    final public function getCache($namespace)
    {
        return $this->cache;
    }

    public function _removeClassCache()
    {
        $childClass = $this->getReflection()->getName();
        $this->cache->clean(array(Cache::TAGS => array($childClass)));

        return;
    }

    public function _removeItemCache($id)
    {
        $childClass = $this->getReflection()->getName();
        unset($this->cache[$childClass . $id]);

        return;
    }

    /**
     * @return dibiRows cached result
     */
    public function _getAllRecords()
    {
        $childClass = $this->getReflection()->getName();

        $cacheKey = $this->getReflection()->getName() . __FUNCTION__;

        if (isset($this->cache[$cacheKey]))
            return $this->cache[$cacheKey];

        $result = $this->_getRecords()->orderBy('id ASC')->fetchAll();
        $this->cache->save($cacheKey, $result, array(Cache::TAGS => array($childClass)));

        return $result;
    }

    /**
     * @return array from cached results
     */
    public function _getPairsFromRecords()
    {
        $returnArray = array();
        $records = $this->_getAllRecords();

        foreach ($records as $record)
            $returnArray[$record->id] = $record->title;

        return $returnArray;
    }

    /**
     * @param int $id
     * @return dibiRow cached result
     */
    public function _getRecord($id)
    {
        $childClass = $this->getReflection()->getName();
        $key = $childClass . $id;

        if (isset($this->cache[$key]))
            return $this->cache[$key];

        $result = $this->_getRecords()->where('id = %i', $id)->fetch();

        $this->cache->save($key, $result, array(
            Cache::TAGS => array($childClass, $key)
        ));

        return $result;
    }

    /**
     * @return dibiDataSource
     */
    public function _getRecords($selectFields = NULL)
    {
        $dibiDataSource = $this->dibi->select('*')->from($this->_getTable());
        if ($selectFields)
            $dibiDataSource->select(FALSE)->select($selectFields);

        return $dibiDataSource;
    }

    /**
     * @return dibiDataSource
     */
    public function _insert($args)
    {
        return $this->dibi->insert($this->_getTable(), $args);
    }

    /**
     * @return dibiDataSource
     */
    public function _update($args)
    {
        return $this->dibi->update($this->_getTable(), $args);
    }

    /**
     * @return dibiDataSource
     */
    public function _delete()
    {
        return $this->dibi->delete($this->_getTable());
    }

    public function _getTable()
    {
        return $this->_getChildStaticProperty('table');
    }

    /**
     * @param type $propertyName
     * @return mixed static variable value 
     */
    public static function _getChildStaticProperty($propertyName)
    {
        $properties = self::getReflection()->getStaticProperties();
        if (array_key_exists($propertyName, $properties))
            return $properties[$propertyName];

        throw new \ReflectionException('Class ' . self::getReflection()->getName() . ' does not have a static property named ' . $propertyName);
    }

}
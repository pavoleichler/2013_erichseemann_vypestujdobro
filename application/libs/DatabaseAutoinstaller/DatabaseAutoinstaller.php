<?php

/**
 * Executes the SQL queries as they are appended to a provided SQL file.
 * Useful to automatically run SQL install scripts containing the DB structure.
 */
class DatabaseAutoinstaller {
    
    protected $database;
    protected $environment;
    protected $silent;
    
    protected $storageFile = 'database-autoinstaller_status.json';
    protected $storage;
    
    protected $structureFile;
    
    protected $status = null;
    protected $firstInstall = false;

    /**
     * Executes the SQL queries as they are appended to a provided SQL file.
     * 
     * @param Nette\Database\Connection $database Nette database service to execute the queries on.
     * @param string $storage Path to a storage directory to be used by the object.
     * @param string $structureFile Path to a SQL file to monitor and execute queries from.
     * @param string $environment A unique name for the envirnonment the application is currently used on. Useful to distinguish multiple envrionments, e.g. produciton / test server.
     * @param boolean $silent Optional. If set to true, a PHP warning is issued on SQL error, otherwise an exception is throwned. Default true.
     */
    public function __construct(Nette\Database\Connection $database, $storage, $structureFile, $environment = 'production', $silent = true) {
        
        $this->database = $database;
        $this->environment = $environment;
        $this->silent = $silent;
        $this->storage = $storage;
        $this->structureFile = $structureFile;
        
    }
    
    /**
     * Check for changes in the install SQL script and execute them.
     * 
     * @return boolean True if changes were executed sucesfully, false on error or no changes.
     * @throws Exception
     */
    public function install() {
        
        // load installation status from previous calls, if any
        $status = $this->loadStatus();
        
        // no new statements
        if (!$this->structureHasChanged())
            return false;
        
        // new statements exist, we are goning to write to the DB
        // lock the status file exclusively
        $this->lockStorageFile();
        
        // reload the status
        $this->loadStatus();
        // if the status has changed already, another thread is already making the changes, do nothing
        if ($status['bytes'] != $this->status['bytes'] OR
            $status['last_update'] != $this->status['last_update']){
            $this->unlockStorageFile();
            return false;
        }
        
        // get all newly appended queries
        $sql = $this->getStructureDifference();
        
        // execute the queries
        if ($sql)
            $this->executeQueries($sql, $this->firstInstall);
        
        // update status
        $this->updateStatus();
        
        // unlock the file
        $this->unlockStorageFile();
        
        return true;
        
    }
    
    protected function executeQueries($queries, $ignoreDupplicates = false) {
        
        // try to execute the queries
        try{
            
            $this->database->exec($queries);
            
        }catch(PDOException $pdo){
            // the query has failed
            // if this seems to be an error due to missing status storage when the same install script is being executed twice,
            // save the current status as processed and show the error / warning message only once
            if($pdo->getCode() == '42S01' /* table already exists */ && $ignoreDupplicates)
                $this->updateStatus();
            
            // notify the developer
            if ($this->silent){
                // fail silently with a warning only
                trigger_error ('DatabaseAutoinstaller SQL execution failed. '.$pdo->getMessage(), E_USER_WARNING);
                return false;
            }else{
                // we are allowed to throw an exception, be verbose
                $exception = new Exception('DatabaseAutoinstaller SQL execution failed.', 1, $pdo);
                throw $exception;
            }
        }
        
        return true;
        
    }

    /**
     * Check if the structure has changed
     * 
     * @return boolean True if the structure file has changed since the last run, false otherwise.
     */
    protected function structureHasChanged() {
        
        // the file was not modified since the last update
        if (filemtime($this->structureFile) <= $this->status['last_update'] AND 
            filesize($this->structureFile) == $this->status['bytes'])
            return false;
        
        return true;
        
    }

    /**
     * Get all new SQL appended to the structure file since the last update.
     * 
     * @return string|boolean Appended SQL on success, false on failure.
     */
    protected function getStructureDifference() {
        
        // open the file
        if (!($file = fopen($this->structureFile, 'rb')))
            return false;
        
        // if the file has shrinked, return an empty string
        $filesize = filesize($this->structureFile);
        if ($filesize < $this->status['bytes'])
            return false;
        
        // skip to the new bytes
        fseek($file, $this->status['bytes']);
        
        // load the rest of the file
        $difference = fread($file, $filesize);
        $difference = trim($difference);
        
        // close the file
        fclose($file);
        
        return strlen($difference) ? $difference : false;
        
    }

    /**
     * Get a path to the storage file.
     * 
     * @return string Path to the storage file.
     */
    protected function getStorageFileName(){
        
        // create the directory if it does not exist
        if (!file_exists($this->storage))
            mkdir($this->storage);
        
        $environment = preg_replace('[^a-zA-Z0-9\-_]', '', $this->environment);
        
        return $this->storage . '/' . $environment . '_' . $this->storageFile;
        
    }
    
    /**
     * Create an exclusive lock on the storage file.
     */
    protected function lockStorageFile() {
        
        // create the file if it does not exist yet
        $mode = file_exists($this->getStorageFileName()) ? 'r' : 'w';
            
        $file = fopen($this->getStorageFileName(), $mode);
        flock($file, LOCK_EX);
        fclose($file);
        
    }
    
    /**
     * Releast the lock on the storage file.
     */
    protected function unlockStorageFile() {
        
        $file = fopen($this->getStorageFileName(), 'r');
        flock($file, LOCK_UN);
        fclose($file);
        
    }
    
    /**
     * Update status with the current values. 
     */
    protected function updateStatus() {
        
        $this->status['last_update'] = time();
        $this->status['bytes'] = filesize($this->structureFile);
        $this->saveStatus();
        
    }

    /**
     * Save status variable to the storage file.
     */
    protected function saveStatus() {
        
        $encoded = json_encode($this->status);
        
        file_put_contents($this->getStorageFileName(), $encoded);
    }
    
    /**
     * Load staus variable from the storage file.
     * 
     * @return array Status.
     */
    protected function loadStatus() {
        
        // set default status values, if the last status is not provided
        if (!file_exists($this->getStorageFileName())){
            $this->status = array('last_update' => 0, 'bytes' => 0);
            $this->firstInstall = true;
            return $this->status;
        }
        
        // load status and parse it
        $encoded = file_get_contents($this->getStorageFileName());
        $this->status = json_decode($encoded, true);
        
        // set default status values, if the last status is not provided
        if (!$this->status OR
            !isset($this->status['last_update']) OR
            !isset($this->status['bytes'])){
            $this->status = array('last_update' => 0, 'bytes' => 0);
            $this->firstInstall = true;
        }else{
            $this->firstInstall = false;
        }
        
        return $this->status;
        
    }
    
}

?>
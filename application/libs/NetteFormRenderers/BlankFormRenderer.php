<?php

namespace Nette\Forms\Rendering;

use Nette, Nette\Utils\Html;

/**
 * Twitter Bootstrap compatible horizontal form renderer.
 */
class BlankFormRenderer extends DefaultFormRenderer 
{
	 /*  /--- form.container
	 *
	 *    /--- if (form.errors) error.container
	 *      .... error.item [.class]
	 *    \---
	 *
	 *    /--- hidden.container
	 *      .... HIDDEN CONTROLS
	 *    \---
	 *
	 *    /--- group.container
	 *      .... group.label
	 *      .... group.description
	 *
	 *      /--- controls.container
	 *
	 *        /--- pair.container [.required .optional .odd]
	 *
	 *          /--- label.container
	 *            .... LABEL
	 *            .... label.suffix
	 *            .... label.requiredsuffix
	 *          \---
	 *
	 *          /--- control.container [.odd]
	 *            .... CONTROL [.required .text .password .file .submit .button]
	 *            .... control.requiredsuffix
	 *            .... control.description
	 *            .... if (control.errors) error.container
	 *          \---
	 *        \---
	 *      \---
	 *    \---
	 *  \--
	 *
	 * @var array of HTML tags */
	public $wrappers = array(
		'form' => array(
			'container' => NULL,
			'errors' => TRUE,
		),

		'error' => array(
			'container' => 'span',
			'item' => 'span class="alert alert-error"',
		),

		'group' => array(
			'container' => 'fieldset',
			'label' => 'legend',
			'description' => 'span',
		),

		'controls' => array(
			'container' => NULL,
		),

		'pair' => array(
			'container' => NULL,
			'.required' => NULL,
			'.optional' => NULL,
			'.odd' => NULL,
		),

		'control' => array(
			'container' => NULL,
			'.odd' => NULL,

			'errors' => FALSE,
			'description' => 'small',
			'requiredsuffix' => '',

			'.required' => 'required',
			'.text' => 'text',
			'.password' => 'text',
			'.file' => 'text',
			'.submit' => 'button btn-success',
			'.image' => 'imagebutton',
			'.button' => 'button btn',
		),

		'label' => array(
			'container' => NULL,
			'suffix' => NULL,
			'requiredsuffix' => '',
		),

		'hidden' => array(
			'container' => NULL,
		),
	);

}

?>
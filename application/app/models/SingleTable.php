<?php

namespace Models;

abstract class SingleTable extends Database
{
    
    /** @var string */
    protected $table;
    
    /** 
     * Autoincrement column name. Set as null, if no autoincrement column exists.
     * @var string
     */
    protected $autoIncrement = 'id';


    /**
     * 
     * @param \DibiConnection $dibi
     * @param string $table
     */
    public function __construct(\DibiConnection $dibi, $table){
        parent::__construct($dibi);
        
        $this->table = $table;
        
    }
    
    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * 
     *                               Configuration
     * 
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
    
    /**
     * Public table name getter.
     * 
     * @return string
     */
    public function getTable() {
        
        return $this->table;
        
    }
    
    /**
     * Public table name setter.
     * 
     * @param string $table
     */
    public function setTable($table) {
        
        $this->table = $table;
        
    }

    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * 
     *                             Model behaviour
     * 
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
    
    /**
     * Formats the basic select. Do not call it zourself, use select() instead.
     * 
     * @return \DibiFluent
     */
    protected function base() {
        
        return clone $this->dibi->select('*')->from($this->table);
        
    }
    
    /**
     * Fetches the rows in a given format.
     * 
     * @param \DibiFluent $fluent
     * @return \DibiResult The returned rows.
     */
    protected function fetch(\DibiFluent $fluent) {
        
        return $fluent->fetchAll();
        
    }

    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * 
     *                                  Selects
     * 
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

    /**
     * Returns the basic select.
     * Call withou arguments to retrieve the default fields, or pass custom fields in the same way, you would call the dibi::select() method.
     * 
     * @return \DibiFluent
     */
    protected function select() {
        
        $args = func_get_args();
        
        if (count($args))
            // overrides the default select fields
            return clone call_user_func_array (array($this->base()->select(false), 'select'), $args);
        else
            return clone $this->base();
        
    }
    
    

    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * 
     *                             Where conditions
     * 
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
    
    /**
     * Adds an equal where condition to a DibiFluent object.
     * 
     * @param \DibiFluent $fluent
     * @param stirng|array $column
     * @param mixed $value
     * @return \DibiFluent
     */
    protected function whereEqual($fluent, $column, $value) {
        
        if (is_array($value))
            $fluent->where('%n IN %in', $this->column($column), $value);
        else
            $fluent->where('%n = %s', $this->column($column), $value);
        
        return $fluent;
        
    }

    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * 
     *                                 Counting
     * 
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

    /**
     * Counts all rows.
     * 
     * @return int
     */
    public function count() {
        
        return count($this->select());
        
    }

    /**
     * Counts all distinct rows.
     * 
     * @return int
     */
    public function countDistinct($column) {
        
        return $this->select('COUNT(DISTINCT %n)', $this->column($column))->fetchSingle();
        
    }

    /**
     * Counts the number of rows having a column equal to the given value or get an array of row counts grouped by the $column.
     * 
     * @param mixed $column Column name or an array with 2 keys - table name and column name.
     * @param mixed $value Column value.
     * @return int|\DibiResult Row count or the \DibiResult object with row count for each column value.
     */
    public function countBy($column, $value = null) {
        
        // get the rows
        $select = $this->select('%n, COUNT(*) AS count', $this->column($column));
        
        if ($value === null){
            return $select->groupBy($this->column($column))->fetchAll();
        }else{
            return count($this->whereEqual ($select, $column, $value));
        }
        
    }

    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * 
     *                            Getting single row
     * 
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
    
    /**
     * Get one row.
     * 
     * @param string $order use of the predefined object constants.
     * @param int $limit
     * @param int $offset
     * @return \DibiRow Found row or false.
     */
    public function one($order = null) {
        
        $select = $this->select()->limit(1);
        
        if ($order !== null)
            $select->orderBy($order);
        
        return $this->fetchOne($select);
        
    }

    /**
     * Look up the table by a given column value and return a single row.
     * 
     * @param mixed $column Column name or an array with 2 keys - table name and column name.
     * @param mixed $value Column value.
     * @param string $order Order by.
     * @param int $limit Limit.
     * @param int $offset Offset
     * @return \DibiRow Found row or false.
     */
    public function oneBy($column, $value, $order = null) {
        
        $select = $this->whereEqual($this->select()->limit(1), $column, $value);
        
        if ($order !== null)
            $select->orderBy ($order);
        
        return $this->fetchOne($select);
        
    }
    
    /**
     * Gets the row by its autoincrement value.
     * 
     * @param int $value
     */
    public function oneByAutoIncrement($value) {

        $autoIncrement = $this->autoIncrement;
        
        $select = $this->select()->where('%n = %i', $autoIncrement, $value)->limit(1);
        
        return $this->fetchOne($select);
        
    }
    
    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * 
     *                            Getting multiple rows
     * 
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

    /**
     * Get all rows.
     * 
     * @param string $order use of the predefined object constants.
     * @param int $limit
     * @param int $offset
     * @return \DibiResult Found rows.
     */
    public function get($order = null, $limit = 50, $offset = 0) {
        
        $select = $this->select();
        
        if ($order !== null)
            $select->orderBy($order);
        
        if ($limit !== null)
            $select->limit($limit);
        
        if ($offset !== null)
            $select->offset($offset);
        
        return $this->fetchAll($select);
        
    }

    /**
     * Look up the table by a given column value.
     * 
     * @param mixed $column Column name or an array with 2 keys - table name and column name.
     * @param mixed $value Column value.
     * @param string $order Order by.
     * @param int $limit Limit.
     * @param int $offset Offset
     * @return \DibiFluent
     */
    public function getBy($column, $value, $order = null, $limit = null, $offset = null) {
        
        $select = $this->whereEqual($this->select(), $column, $value);
        
        if ($order !== null)
            $select->orderBy($order);
        
        if ($limit !== null)
            $select->limit($limit);
        
        if ($offset !== null)
            $select->offset($offset);
        
        return $this->fetchAll($select);
        
    }

    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * 
     *                               Adding rows
     * 
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
    
    /**
     * Inserts a row to the table.
     * 
     * @param array $values Values for the row to be inserted.
     * @param array $columns An array of allowed columns to be specified in the insert.
     * @return \DibiRow If an autoincrement column exists, inserted row.
     */
    public function add($values, $columns = null) {
        
        if ($columns !== NULL)
            $values = $this->filterValues($values, $columns);
        
        $insert = $this->dibi->insert($this->table(), $values);
        
        if ($this->autoIncrement){
            // autoincrement column exists
            $id = $insert->execute(\dibi::IDENTIFIER);
            return $this->oneByAutoIncrement($id);
        }else{
            // no autoincrement column
            // TODO get the row by a unique key
            $result = $insert->execute();
            return $result;
        }
        
    }

    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * 
     *                              Modyfing rows
     * 
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
    
    /**
     * Update rows with a given column values.
     * 
     * @param mixed $column Column name or an array with 2 keys - table name and column name.
     * @param mixed $value Value to choose rows.
     * @param array $values Values to update the row to.
     * @param array $columns Columns to update.
     * @return \DibiResult|int If the $column value was not modified by this update, returns the updated rows.
     */
    public function modifyBy($column, $value, $values, $columns = null) {

        // filter the values to update, if requested
        if ($columns)
            $values = $this->filterValues ($values, $columns);
        
        // update matching rows
        $update = $this->dibi->update($this->table(), $values);
        $result = $this->whereEqual($update, $column, $value)->execute();

        // if the query did not affect the $column value, return the affected rows
        // otherwise return the update call result
        // TODO return the current state of the rows
        if (array_key_exists($column, $values)){
            return $result;
        }else{
            $rows = $this->whereEqual($this->select(), $column, $value);
            return $this->fetchAll($rows);
        }
        
    }
    
    /**
     * Update or insert a column with the given value.
     * 
     * @param mixed $column Column name or an array with 2 keys - table name and column name.
     * @param mixed $value Value. If null, the row will be inserted.
     * @param array $values
     * @param array $columns
     * @return \DibiFluent|\DibiResult|int
     */
    public function saveBy($column, $value, $values, $columns = null) {
        
        if ($value === null){
            // no value specified, insert a new row
            return $this->add($values, $columns);
        }else{
            // value is defined, update the matching rows
            return $this->updateBy($column, $value, $values, $columns);
        }
        
    }

    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * 
     *                              Deleting rows
     * 
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
    
    /**
     * Delete rows with a given column value.
     * 
     * @param mixed $column Column name or an array with 2 keys - table name and column name.
     * @param mixed $value Value.
     * @return mixed
     */
    public function deleteBy($column, $value) {
        
        // delete matching rows
        $delete = $this->dibi->delete($this->table());
        $result = $this->whereEqual($delete, $column, $value)->execute();

        return $result;
        
    }

    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * 
     *                                 Tools
     * 
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
    
    /**
     * Returns the table name.
     * 
     * @return string
     */
    protected function table() {
        
        return $this->table;
        
    }
    
    /**
     * Returns a column identifier string, specific enough to prevent ambigous resolution.
     * 
     * @param string|array $column Column name or an array with 2 keys - table name and column name.
     * @return array
     */
    protected function column($column) {
        
        // simple column name in a string without a table name
        if (!is_array($column))
            return $this->table() . '.' . $column;
        
        // simple column name as a single array key
        if (count($column) < 2)
            array_unshift ($column, $this->table());
        
        // an array with 2 keys - a table and a column name
        return implode('.', array_slice($column, 0, 2));
        
    }
    
    /**
     * Fetches one row.
     * 
     * @param \DibiFluent $fluent
     * @return \DibiRow The returned row.
     */
    protected function fetchOne(\DibiFluent $fluent) {
        
        $rows = $this->fetch($fluent);
        
        return $rows ? reset($rows) : $rows;
        
    }
    
    /**
     * Fetches all rows.
     * 
     * @param \DibiFluent $fluent
     * @return \DibiResult The returned rows.
     */
    protected function fetchAll(\DibiFluent $fluent) {
        
        return $this->fetch($fluent);
        
    }

    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * 
     *                              Magic
     * 
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

    /**
     * Magic method allows the object to be called with methods in the following format:
     * countBy[ColumnName]($columnValue)
     * countDistinctBy[ColumnName]($columnValue)
     * oneBy[ColumnName]($columnValue, $order)
     * getBy[ColumnName]($columnValue, $order, $limit = null, $offset = null)
     * updateBy[ColumnName]($columnValue, $values, $columnsToUpdate = null)
     * removeBy[ColumnName]($columnValue)
     * saveBy[ColumnName]($columnValue = null, $values, $columnsToUpdate = null)
     * 
     * @param string $name
     * @param array $args
     * @return \DibiFluent
     * @throws \Nette\MemberAccessException
     * @throws \Nette\InvalidArgumentException
     */
    public function __call($name, $args) {
        
        // parse the method name and explode basic parameters
        $valid = preg_match('/^([a-zA-Z]+)By([A-Za-z0-9]+)/', $name, $matches);
        if (!$valid)
            throw new \Nette\MemberAccessException("Call to undefined method $name.");
        $action = $matches[1];
        $columnName = $matches[2];
        
        // resolve the provided column name and find out the exact database column name
        $column = $this->resolveColumn($columnName);
        
        switch ($action) {
            
            case 'count':
                
                // call a countBy method
                array_unshift($args, $column);
                return call_user_func_array(callback($this, 'countBy'), $args);
            
            case 'countDistinct':
                
                // call a countDistinct method
                array_unshift($args, $column);
                return call_user_func_array(callback($this, 'countDistinct'), $args);
            
            case 'one':
                
                // call a oneBy method
                array_unshift($args, $column);
                return call_user_func_array(callback($this, 'oneBy'), $args);
                
            case 'get':
                
                // call a getBy method
                array_unshift($args, $column);
                return call_user_func_array(callback($this, 'getBy'), $args);
                
            
            case 'update':
                
                // call an updateBy method
                array_unshift($args, $column);
                return call_user_func_array(callback($this, 'updateBy'), $args);
            
            case 'remove':
                
                // call a removeBy method
                array_unshift($args, $column);
                return call_user_func_array(callback($this, 'removeBy'), $args);
            
            case 'save':
                
                // call a saveBy method
                array_unshift($args, $column);
                return call_user_func_array(callback($this, 'saveBy'), $args);
            
            default:
                
                // unknown action name
                throw new \Nette\MemberAccessException("Call to undefined method $name.");
                
        }
    
    }
        
    /**
     * Translates a camel-cased column name provided in a magic method call to a real database column name.
     * 
     * @param string $name Camel-cased name.
     * @return string Real name.
     * @throws \Nette\InvalidArgumentException
     */
    protected function resolveColumn($name){

        // validate the provided column name
        $invalid = preg_match('/[^A-Za-z0-9]/', $name);
        if ($invalid)
            throw new \Nette\InvalidArgumentException("Only alphanumeric characters are allowed in the column name.");
        
        // split the string by words, each one starting with a upper case letter
        preg_match_all('/[A-Z][a-z0-9]*/', $name, $matches);
        $words = $matches[0];
        
        // lowercase the words
        array_walk($words, function(&$word){
            $word = lcfirst($word);
        });
        
        // join words with underscores
        $column = implode('_', $words);
        
        return $column;
        

    }

}
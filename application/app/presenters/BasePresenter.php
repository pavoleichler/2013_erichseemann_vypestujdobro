<?php

/**
 * Base presenter.
 *
 * @property-read \SystemContainer $context
 */
abstract class BasePresenter extends Nette\Application\UI\Presenter
{
    
    /**
     * @persistent
     */
    public $lang;

    protected function startup()
    {
        parent::startup();
        
        if (isset($this->context->users))
            // registers the authenticator to verify users creadentials to
            $this->user->setAuthenticator($this->context->users);
        if (isset($this->context->authorizator))
            // registers the authorizator to verify user role permissions
            $this->user->setAuthorizator($this->context->authorizator);
        
        // set language
        $this->context->translator->setSection($this->lang);
        $this->template->lang = $this->lang;

        // maintain the DB structure up to date
        if (isset($this->context->databaseAutoinstaller))
            $this->context->databaseAutoinstaller->install();

        // variables to pass to javaScript
        $this->template->javascript = array(
            'baseUrl' => $this->template->baseUrl,
            'basePath' => $this->template->basePath,
            'lang' => $this->template->lang
        );

        // form addons
        \JanTvrdik\Components\DatePicker::register();
        \Nette\Forms\Controls\CheckboxList::register();
    }

    public function beforeRender()
    {
        parent::beforeRender();

        $this->template->setTranslator($this->context->translator);
    }

    public function templatePrepareFilters($template){

        // add {jsmin} macro
        $template->registerFilter($latte = new Nette\Latte\Engine);
        $set = new \Nette\Latte\Macros\MacroSet($latte->compiler);
        $set->addMacro('jsmin', 'ob_start()', '$_jsminContent = ob_get_contents(); ob_end_clean(); echo JSMin::minify($_jsminContent);');

     }    

    /**
     * Component to load minified CSS files.
     * 
     * @return \WebLoader\Nette\CssLoader
     */
    protected function createComponentCss() {

        // add all files in the www/css directory
        $files = new \WebLoader\FileCollection(WWW_DIR . '/css');
        $files->addFiles(\Nette\Utils\Finder::findFiles('*.css')->from(WWW_DIR . '/css'));

        // create compiler object
        $compiler = \WebLoader\Compiler::createCssCompiler($files, WWW_DIR . '/data');

        // create CSS loader control
        return new \WebLoader\Nette\CssLoader($compiler, $this->template->basePath . '/data');
    }

    /**
     * Component to load minified JS files.
     * 
     * @return \WebLoader\Nette\CssLoader
     */
    protected function createComponentJs(){

        // add all files in the www/js directory
        $files = new \WebLoader\FileCollection(WWW_DIR . '/js');
        $files->addFiles(\Nette\Utils\Finder::findFiles('*.js')->from(WWW_DIR . '/js'));

        // create compiler object
        $compiler = \WebLoader\Compiler::createJsCompiler($files, WWW_DIR . '/data');
        $compiler->addFilter(function ($code) {
                    return JSMin::minify($code);
        });

        // create JS loader control
        return new \WebLoader\Nette\JavaScriptLoader($compiler, $this->template->basePath . '/data');
    }

}

Application = {
    /**
     * Called on page <head> load.
     */
    init: function(){

        this.ContactForm.init();
        this.Header.init();
        this.Illustration.init();
        this.Scroller.init();
        this.Nette.init();
        this.Facebook.init(this.Analytics);
        this.Twitter.init(this.Analytics);

        $(function(){

            // called on DOM ready


        });

    },
    ContactForm: {
        element: 'form.contact',
        init: function(){
        
            var self = this;
            
            // forms with class .ajax are submitted by AJAX
            $(this.element).live('submit ajaxsubmit', function(event) {
                self.onSubmit();
            });
            
        },
        onSubmit: function(){
            $(this.element).addClass('submitted');
        }
    },
    Illustration: {
        element: '#perex .vegetables',
        property: 'margin-bottom',
        maxDeviation: 50, // maximal deviation of the element in px
        maxOffset: 660, // maximal offset of mouse in px
        mouse: {x: 0, y: 0},
        scroll: 0,
        init: function(){

            var self = this;

            $(window).mousemove(function(event){
                self.update($(window).scrollTop(), {x: event.pageX, y: event.pageY});
                self.render();
            });

            $(window).scroll(function(){
                self.update($(window).scrollTop());
                self.render();
            });

        },
        update: function(scroll, mouse){

            // get scroll difference
            var scrollDelta = scroll - this.scroll;

            // save current scroll value
            this.scroll = scroll;

            if (typeof mouse != 'undefined'){
                // save current mouse position
                this.mouse = mouse;
            }else{
                // mouse position not provided, estimate
                this.mouse.y += scrollDelta;
            }


        },
        render: function(){

            var percent = this.mouse.y / this.maxOffset;
            percent = percent > 1 ? 1 : percent; // 0 - 1 value based on the current Y axis mouse position
            var coefficient = percent * percent; // exponential value
            var offset = coefficient * this.maxDeviation;

            $(this.element).css(this.property, Math.floor(offset) + 'px');

        }
    },
    /* Collapses the main header when the page is scrolled down. */
    Header: {
        max: 45, // maximal height to collapse in px
        element: 'header.main',
        init: function(){

            var self = this;

            $(window).scroll(function(){
                self.update();
            });

        },
        update: function(){

            var scroll = $(window).scrollTop();
            var offset = this.max < scroll ? this.max : scroll;

            $(this.element).css('top', - offset + 'px');

        }
    },
    /**
     * Google analytivs related tools
     */
    Analytics: {
        // track page view
        track: function(page){

            // init the GA queue array, if it is missing
            if (typeof _gaq == 'undefined')
                window._gaq = [];

            _gaq.push(['_trackPageview', page]);

        },
        // tirgger event
        trigger: function(category, action, label, value, nonInteraction){

            // init the GA queue array, if it is missing
            if (typeof _gaq == 'undefined')
                window._gaq = [];

            _gaq.push(['_trackEvent', category, action, label, value, nonInteraction]);

        }
    },
    /**
     * Twitter related tools.
     */
    Twitter: {
        analytics: null,
        init: function(analytics){

            // save analytics object
            this.analytics = analytics;

            // init child objects
            this.Share.init(this);

            // create shortcuts for most useful methods
            this.share = this.Share.open;

        },
        Share: {
            twitter: null,
            elements: '.twitter-share',
            init: function(twitter){

                var self = this;

                this.twitter = twitter;

                // open the send dialog for the given elements
                $(this.elements).live("click", function (event) {

                    event.preventDefault();

                    var $element = $(this);

                    var properties = {
                        text: $element.attr('data-text'),
                        url: $element.attr('data-url'),
                        hashtags: $element.attr('data-hashtags'),
                        via: $element.attr('data-via'),
                        related: $element.attr('data-related')
                    };

                    // open
                    self.open.call(self, properties);

                });

            },
            open: function(properties){

                // filter out empty properties
                var variables = $.grep(properties, function(value){
                    if (typeof value != 'undefined' && value !== null)
                        return true;
                });
                // construct the share dialog URL
                var sharer = 'http://twitter.com/share?' + $.prop(variables);

                // configure the new browset window
                var settings = 'height=450, width=550, toolbar=0, location=0, menubar=0, directories=0, scrollbars=0';

                // track dialog openings
                this.twitter.analytics.trigger('Twitter', 'Share', 'open');

                // open the sharer
                window.open(sharer, 'twitter' + new Date().getTime(), settings);

            }
        }
    },
    /**
     * Facebook related tools.
     */
    Facebook: {
        analytics: null,
        init: function(analytics){

            // save analytics object
            this.analytics = analytics;

            // init child objects
            this.Login.init(this);
            this.Share.init(this);
            this.Requests.init(this);
            this.Send.init(this);

            // create shortcuts for most useful methods
            this.share = this.Share.open;
            this.requests = this.Requests.open;
            this.send = this.Send.open;

        },
        Send: {
            facebook: null,
            elements: '.facebook-send',
            init: function(facebook){

                var self = this;

                this.facebook = facebook;

                // open the send dialog for the given elements
                $(this.elements).live("click", function (event) {

                    event.preventDefault();

                    var $element = $(this);

                    var properties = {
                        name: $element.attr('data-name'),
                        description: $element.attr('data-description'),
                        link: $element.attr('data-link'),
                        picture: $element.attr('data-picture'),
                        to: $element.attr('data-to')
                    };

                    // open
                    self.open.call(self, properties);

                });

            },
            open: function(properties){

                var self = this;

                properties = $.extend({method: 'send'}, properties);

                this.facebook.analytics.trigger('Facebook', 'Send', 'open');

                FB.ui(properties, function(response) {
                    if (response && response.post_id) {
                        self.facebook.analytics.trigger('Facebook', 'Send', 'success');
                    }
                });

            }
        },
        Requests: {
            facebook: null,
            elements: '.facebook-requests',
            init: function(facebook){

                var self = this;

                this.facebook = facebook;

                // open the requests dialog for the given elements
                $(this.elements).live("click", function (event) {

                    event.preventDefault();

                    var $element = $(this);

                    var properties = {
                        message: $element.attr('data-message')
                    };

                    // open
                    self.open.call(self, properties);

                });

            },
            open: function(properties){

                var self = this;

                properties = $.extend({method: 'apprequests'}, properties);

                // track dialog opening
                this.facebook.analytics.trigger('Facebook', 'Requests', 'open');

                FB.ui(properties, function(response) {
                    // track requests success
                    if (response && response.post_id) {
                        self.facebook.analytics.trigger('Facebook', 'Requests', 'success');
                    }
                });

            }
        },
        Share: {
            facebook: null,
            elements: '.facebook-share',
            init: function(facebook){

                var self = this;

                this.facebook = facebook;

                // open the share dialog for the given elements
                $(this.elements).live("click", function (event) {

                    event.preventDefault();

                    var $element = $(this);

                    var properties = {
                        name: $element.attr('data-name'),
                        caption: $element.attr('data-caption'),
                        description: $element.attr('data-description'),
                        link: $element.attr('data-link'),
                        picture: $element.attr('data-picture')
                    };

                    if ($element.attr('data-action-name') && $element.attr('data-action-link')){
                        properties.actions = [{
                            name: $element.attr('data-action-name'),
                            link: $element.attr('data-action-link')
                        }];
                    }

                    // open
                    self.open.call(self, properties);

                });
            },
            open: function(properties){

                var self = this;

                properties = $.extend({method: 'feed'}, properties);

                // track dialog
                this.facebook.analytics.trigger('Facebook', 'Share', 'open');

                FB.ui(properties, function(response) {
                    // track success
                    if (response && response.post_id) {
                        self.facebook.analytics.trigger('Facebook', 'Share', 'success');
                    }
                });

            }
        },
        Login: {
            facebook: null,
            elements: 'a.facebook-login',
            init: function(facebook){

                var self = this;

                this.facebook = facebook;

                // require facebook login before continuing on the given elements
                // works with anchors only
                $(this.elements).live("click", function (event) {

                    event.preventDefault();

                    var $element = $(this);

                    // require login
                    self.require(function(){
                        // redirect
                        location.href = $element.attr('href');
                    });

                });

            },
            /**
             * Calls the callback only if the user logs in.
             */
            require: function(callback){

                var self = this;

                FB.getLoginStatus(function(response){
                    if (response.status == 'connected'){
                        // logged in
                        callback();
                    }else{
                        // track opening
                        self.facebook.analytics.trigger('Facebook', 'Login', 'open');
                        // open dialog
                        FB.login(function(response){
                            if (response.authResponse){
                                // logged in
                                // track result
                                self.facebook.analytics.trigger('Facebook', 'Login', 'success');
                                // call the callback
                                callback();
                            }else{
                                // logged out
                                // track result
                                self.facebook.analytics.trigger('Facebook', 'Login', 'failure');
                            }
                        });
                    }
                });

            }
        }
    },
    /**
     * Nette related tools.
     */
    Nette: {
        init: function(){

            // links with class .ajax use AJAX
            $("a.ajax").live("click", function (event) {
                event.preventDefault();
                $.get(this.href);
            });

            // forms with class .ajax are submitted by AJAX
            $("form.ajax").live('submit', function(event) {
                event.preventDefault();
                var request = $(this).ajaxSubmit();
                if (request){
                    $(this).trigger('ajaxsubmit');
                }
            });

        }
    },
    /*
     * Scrolls smoothly to the anchor links.
     */
    Scroller: {
        speed: 500,
        init: function(){

            var self = this;

            $('a[href^=#]').live('click', function(event){

                // get the href attribute
                var href = $(this).attr('href');
                // ignore sharp only links
                if (href == '#')
                    return;

                // extract the target ID
                var id = href.substring(1);

                // prevent default action
                event.preventDefault();

                // scroll to the element
                var result = self.scrollTo(id);

                // add the hash id to the location bat
                location.href = '#' + id;

            });

        },
        scrollTo: function(target, speed){

            var offset = null;

            if (target instanceof $){
                // scroll to element
                offset = target.offset().top;
            }else if (typeof target == 'number'){
                // scroll to px
                offset = target;
            }else{
                // scroll to ID
                // look for a named anchor
                var $element = $('a[name=' + target + ']');
                // if not found, look for an element with the given ID
                if (!$element.length)
                    $element = $('#' + target);
                offset = $element.length ? $element.offset().top : null;
            }

            // use default speed if not provided
            speed = (typeof speed == 'undefined') ? this.speed : speed;

            // smooth scroll
            if (offset !== null)
                $('html, body').animate({scrollTop: offset}, speed);
            else
                return false;

            return true;

        }
    }
}

// Initiate the application
Application.init();
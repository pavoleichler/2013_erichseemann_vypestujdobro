<?php

namespace Models;

abstract class JoinedTable extends SingleTable
{
    
    protected $joins = array();
    
    public function __construct(\DibiConnection $dibi, array $table, array $joins = array()) {
        parent::__construct($dibi, $table);
        
        if (count($this->table) != 1)
            throw new Exception('The main $table should be specified as an array with one alias => real name element.');
        
        // join tables names
        $this->joins = $joins;
        
        if (array_key_exists(key($this->table), $this->joins))
            throw new Exception('One of the join tables has the same alias keys as the main table.');
            
        
    }

    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * 
     *                               Configuration
     * 
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
    
    /**
     * Public join table name getter.
     * 
     * @param type $key Internal table name.
     * @return string
     */
    public function getJoinTable($key) {
        
        return isset($this->joins[$key]) ? $this->joins[$key] : null;
        
    }
    
    /**
     * Public join table name setter.
     * 
     * @param type $key Internal table name. This should not change, even if the real table name does.
     * @param type $name Table name.
     */
    public function setJoinTable($key, $name) {
        
        $this->joins[$key] = $name;
        
    }
    
    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * 
     *                               Tools
     * 
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
    
    /**
     * Gets the name of a table by its key.
     * 
     * @param string $key
     * @return string
     * @throws Exception
     */
    protected function table($key = null, $alias = true) {
        
        // if no name is specified, return the main table name only
        if ($key === null)
            return reset($this->table);
        
        // table by name
        // main table
        if (isset($this->table[$key]))
            return $alias ? $this->table[$key] . ' ' .$key : $this->table[$key];
        // join table
        if (isset($this->joins[$key]))
            return $alias ? $this->joins[$key] . ' ' .$key : $this->joins[$key];
        
        throw new \Exception("Unknown table '$key'.");
        
    }
    
    /**
     * Returns a column identifier string, specific enough to prevent ambious resolution.
     * For columns with no table name specified, the main table alias is prepended.
     * 
     * @param string|array $column Column name or an array with 2 keys - table name and column name.
     * @return array
     */
    protected function column($column) {
        
        if (!is_array($column))
            $column = array(key($this->table), $column);
        
        return parent::column($column);
        
    }

}
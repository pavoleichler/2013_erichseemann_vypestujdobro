#
# Basic DB model for a Facebook tab application login.
#

CREATE TABLE `users` (
  `id` BIGINT(20) UNSIGNED AUTO_INCREMENT NOT NULL,
  `fbid` BIGINT(20) UNSIGNED NOT NULL,
  `name` VARCHAR(255),
  `first_name` VARCHAR(255),
  `last_name` VARCHAR(255),
  `email` VARCHAR(255),
  `liked` TINYINT(1) DEFAULT 0 NOT NULL,
  `created` DATETIME NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_slovak_ci;

CREATE TABLE `friends` (
  `fbid` BIGINT(20) UNSIGNED NOT NULL,
  `user` BIGINT(20) UNSIGNED NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`user`, `fbid`),
  FOREIGN KEY (`user`) REFERENCES `users`(`fbid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_slovak_ci;
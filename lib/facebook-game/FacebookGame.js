function FacebookGame(userProperties){

    var me = this;
    this.userID = null;
    this.user = null;

    this.liked = false;

    this.properties = {
        requireLike: false,
        likeTarget: '',
        likeTargetId: '',
        permissions: ''
    };

    this.properties = $.extend(this.properties, userProperties);

    if (typeof this.properties.unknownUser == 'undefined')
        throw "Missing unknownUser callback.";
    if (this.properties.requireLike && typeof this.properties.dislikedUser == 'undefined')
        throw "Missing dislikedUser callback.";
    if (typeof this.properties.connectedUser == 'undefined')
        throw "Missing connectedUser callback.";

}

FacebookGame.prototype.check = function(){

    var me = this;
        
    FB.getLoginStatus(function(response) {
      if (response.status == 'connected') {
        me.userID = response.authResponse.userID;
        me.connectedUser();
      } else {
        me.unknownUser();
      }
    });

}

FacebookGame.prototype.connectedUser = function(userID){

    var me = this;

    if (!this.userID)
        return false;

    this.getUserInfo(this.userID, function(){
        me.databasifyUser(function(){
            // connected and saved
            if (me.properties.requireLike)
                me.requireLike();
            else
                me.loadGame();
        }, function(){
            // connected and save failed
            me.databaseConnectionError();
        });
    });

}

FacebookGame.prototype.unknownUser = function(){

    this.properties.unknownUser();

}

FacebookGame.prototype.requireLike = function(){

    var me = this;

    FB.api('/' + this.user.id + '/likes', function(response){
        if (!response || !response.data){
            me.waitForLike();
            return;
        }
        
        for (var z = 0; z < response.data.length; z++){
            if (response.data[z].id == me.properties.likeTargetId){
                me.loadGame();
                return;
            }
        }

        me.waitForLike();
        
    });

}

FacebookGame.prototype.waitForLike = function(){

    var me = this;

    FB.Event.subscribe('edge.create', function(response) {
        if (response == me.properties.likeTarget)
            me.loadGame();
    });

    this.properties.dislikedUser();

}

FacebookGame.prototype.databaseConnectionError = function(){

    if (typeof this.properties.dbConnectionError != 'undefined')
        this.properties.dbConnectionError();
    else
        alert('Ospravedlňujeme sa, zlyhalo pripojenie k databáze. Skúste prosím znovu načítať stránku.');

}

FacebookGame.prototype.loadGame = function(){

    this.properties.connectedUser();

}

FacebookGame.prototype.databasifyUser = function(success, failure){
    
    var properties = {
        databasifyUser: Math.floor(Math.random() * 10000),
        user: this.user
    };

    $.getJSON('saveUser', properties, function(data){
        if (data && data.result == true)
            success();
        else
            failure();
    });

}

FacebookGame.prototype.getUserInfo = function(id, callback){
    
    var me = this;

    FB.api('/' + id, function(response) {
        me.user = response;
        callback(response);
    });

}

FacebookGame.prototype.login = function(success, failure){

    var me = this;

    FB.login(function(response) {
      if (response.status == 'connected') {
        me.check();
        if (typeof success != 'undefined')
            success();
      } else {
        if (typeof failure != 'undefined')
            failure();
      }
    }, {scope: me.properties.scope});

}

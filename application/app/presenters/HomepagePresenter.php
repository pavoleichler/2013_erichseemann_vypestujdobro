<?php

/**
 * Homepage presenter.
 *
 * @author     Pavol Eichler
 */
class HomepagePresenter extends BasePresenter
{

    protected function startup() {
        parent::startup();

        // meta data
        $this->template->title = $this->context->translator->translate('Vypestujme spoločne dobro');

        $this->template->meta = array(
            'name' => array(
                'description' => $this->context->translator->translate('Pomôžte kúskom svojej úrody ľuďom v ťažkej situácii.'),
                'keywords' => $this->context->translator->translate(''),
            ),
                'property' => array(
                'og' => array(
                    'type' => 'website',
                    'title' => $this->context->translator->translate('Vypestujme spoločne dobro'),
                    'description' => $this->context->translator->translate('Pomôžte kúskom svojej úrody ľuďom v ťažkej situácii.'),
                    'image' => $this->template->baseUrl . '/i/profile.png',
                    'url' => $this->link('//default')
                ),
                'fb' => array(
                    'app_id' => $this->context->params['facebook']['appId']
                )
            )
        );

        // facebook set up
        $this->template->facebook = array(
            'appId' => $this->context->params['facebook']['appId'],
            'locale' => $this->context->params['facebook']['locale']
        );

        // analytics set up
        $this->template->analytics = array(
            'id' => $this->context->params['analytics']['id']
        );

        // invalidate popup snippets for ajax calls
        if ($this->isAjax()){
            $this->invalidateControl('popups');
            $this->invalidateControl('contact');
        }

    }

    public function actionVyvyseneZahony() {

        $this->redirectUrl('http://pinterest.com/peterazor/vyvýšené-záhony/');

    }

    public function actionNapisteNam($subject) {

        $this->template->contact = $subject;

        $this->setView('default');

    }

    protected function createComponentContactForm($name) {

        $form = new \Nette\Application\UI\Form($this, $name);
        $form->addText('email', 'Váš email');
        $form->addTextArea('message', 'Text správy')->setRequired('Pred odoslaním, prosím, napíšte text správy.');
        $form->addHidden('subject');
        $form->addSubmit('submit')->setAttribute('class', 'button');

        $form->onSuccess[] = callback($this, 'onContactFormSuccess');

        $form->setTranslator($this->context->translator);
        $form->setRenderer(new \Nette\Forms\Rendering\BlankFormRenderer());

        return $form;
    }

    public function onContactFormSuccess($form) {

        $values = $form->getValues();

        $message = $this->context->nette->createMail();
        $message->setFrom($this->context->parameters['email']['from']);

        // check if this is a predefined email subject
        if (isset($this->context->parameters['email']['predefined'][$values['subject']])){
            $settings = $this->context->parameters['email']['predefined'][$values['subject']];
        }else{
            // it is not, use default
            $settings = $this->context->parameters['email']['predefined']['default'];
        }

        $message->setSubject($settings['subject']);
        $message->addTo($settings['to']);

        $message->setBody("Odosielateľ:\n{$values['email']}\n\n\n{$values['message']}");

        try {
            $message->send();
        } catch (Exception $exc) {
            // failure
            $form->addError('Ľutujeme, ale pri odosielaní vašej správy nastala chyba. Prosím napíšte nám priamo na email alebo nám zavolajte.');
        }

        // success
        $this->template->sent = true;

    }

}
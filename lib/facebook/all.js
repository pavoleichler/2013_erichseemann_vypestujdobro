var FB = {
    /**
     * FB.api makes API calls to the Graph API.
     * 
     * @param path the url path
     * @param method the http method (default "GET")
     * @param params the parameters for the query
     * @param cb the callback function to handle the response
     */
    api: function (path, method, params, cb) {},
    /**
     * FB.ui is a generic helper method for triggering Dialogs that access the Facebook Dialog API endpoint.
     * 
     * @param params The required arguments vary based on the method being used, but specifying the method itself is mandatory. Please find the full list of dialogs that can be used as values for method in the Dialogs documentation. If display is not specified, then iframe dialogs will be used when possible, and popups otherwise.
     * @param {property} method The UI dialog to invoke. (Required)
     * @param {property} display Specify "popup" to force popups. See Dialogs docs for other display types. (Optional)
     * @param cb Optional callback function to handle the result. Not all methods may have a response.
     */
    ui: function (params, cb) {},
    /**
     * FB.getLoginStatus allows you to determine if a user is logged in to Facebook and has authenticated your app.
     * There are three possible states for a user: connected, not_authorized, unknown
     * 
     * @param cb The callback function.
     * @param force Force reloading the login status (default false).
     */
    getLoginStatus: function (cb, force) {},
    /**
     * Synchronous accessor for the current authResponse. The synchronous nature of this method is what sets it apart from the other login methods.
     * You should never use this method at page load time. Generally, it is safer to use FB.getLoginStatus() if you are unsure.
     */
    getAuthResponse: function () {},
    /**
     * Calling FB.login prompts the user to authenticate your application using the OAuth Dialog.
     * 
     * @param cb The callback function.
     * @param opts Options to modify login behavior.
     * @param {property} scope Comma separated list of Extended permissions.
     */
    login: function (cb, opts) {},
    /**
     * Log the user out of your site and Facebook.
     * 
     * @param cb The callback function.
     */
    logout: function (cb) {},
    /**
     * After loading the JavaScript SDK, call FB.init to initialize the SDK with your app ID. This will let you make calls against the Facebook API.
     * If you are using an app ID, all methods must be called after FB.init.
     * 
     * @param options The callback function.
     * @param {property} appId Your application ID.
     * @param {property} cookie true to enable cookie support.
     * @param {property} logging false to disable logging.
     * @param {property} status true to fetch fresh status.
     * @param {property} xfbml true to parse XFBML tags.
     * @param {property} channelUrl Specifies the URL of a custom URL channel file. This file must contain a single script element pointing to the JavaScript SDK URL.
     * @param {property} authResponse Manually set the object retrievable from getAuthResponse.
     * @param {property} frictionlessRequests 
     * @param {property} hideFlashCallback 
     */
    init: function (options) {}
};

FB.Canvas = {
    /**
     * Tells Facebook to resize your iframe. If you do not specific any parameters Facebook will attempt to determine the height of the Canvas app and set the iframe accordingly. If you would like to set the dimension explicitly pass in an object with height and width properties.
     * Note: this method is only enabled when Canvas Height is set to "Settable (Default: 800px)" in the App Dashboard.
     * 
     * @param params
     * @param {property} x
     * @param {property} y
     */
    setSize: function (params) {},
    /**
     * Starts or stops a timer which will grow your iframe to fit the content every few milliseconds. Default timeout is 100ms.
     * Note: this method is only enabled when Canvas Height is set to "Fixed at (800)px" in the App Dashboard.
     * 
     * @param onOrOff
     * @param interval
     */
    setAutoGrow: function (onOrOff, interval) {},
    getPageInfo: function (callback) {},
    /**
     * Tells Facebook to scroll to a specific location of your canvas page. This should be used in conjunction with FB.Canvas.setSize and FB.Canvas.setAutoResize.
     * Note: this method is only enabled when Canvas Height is set to "Settable (Default: 800px)" in the App Dashboard.
     * 
     * @param params
     * @param {property} x
     * @param {property} y
     */
    scrollTo: function (params) {},
    setDoneLoading: function (callback) {},
    startTimer: function () {},
    stopTimer: function (callback) {},
    hideFlashElement: function (elem) {},
    showFlashElement: function (elem) {},
    setUrlHandler: function (callback) {}
};
    
FB.Canvas.Prefetcher = {
    addStaticResource: function () {},
    setCollectionMode: function (mode) {}
};

FB.Event = {
    /**
     * Attaches an handler to an event and invokes your callback when the event fires.
     * 
     * @param name One of the following: auth.logout, auth.prompt, xfbml.render, edge.create, comment.create, comment.remove, message.send
     * @param cb
     */
    subscribe: function (name, cb) {},
    /**
     * Removes handlers on events so that it no longer invokes your callback when the event fires.
     * 
     * @param name One of the following: auth.logout, auth.prompt, xfbml.render, edge.create, comment.create, comment.remove, message.send
     * @param cb
     */
    unsubscribe: function (name, cb) {}
};

FB.XFBML = {
    parse: function (dom, cb) {}
};
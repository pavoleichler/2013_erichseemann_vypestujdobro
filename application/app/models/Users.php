<?php

namespace Models;

/**
 * Users management model.
 */
class Users extends SingleTable implements \Nette\Security\IAuthenticator
{
        
        const INVALID_DATA = 10;
        const USERNAME_EXISTS = 11;
    
        /** \Nette\Database\Table\Selection */
        protected $table;
        /** @var string */
        protected $salt = null;
        /** @var array */
        protected $columns = array(
            'id' => 'id',
            'fbid' => 'fbid',
            'username' => 'username',
            'password' => 'password',
            'role' => 'role'
        );
        
        public function __construct(\DibiConnection $dibi, $salt, $table = 'users', $columns = null) {
            parent::__construct($dibi, $table);
            
            $this->salt = $salt;
            
            // overwrite column names, if requested
            if ($columns)
                $this->columns = array_merge($this->columns, $columns);
            
        }

        /**
	 * Performs an authentication.
	 * @param  array Credentials array.
	 * @return Nette\Security\Identity
	 * @throws Nette\Security\AuthenticationException
	 */
	public function authenticate(array $credentials){

            // find the user in the database
            $row = $this->getByCredentials($credentials);

            // unknown user
            if (!$row)
                throw new \Nette\Security\AuthenticationException("User not found.", self::IDENTITY_NOT_FOUND);

            // create the user identity
            unset($row->{$this->columns['password']});
            
            // create the user
            if ($this->columns['role'] AND isset($row->{$this->columns['role']}))
                $user = new \Nette\Security\Identity($row->{$this->columns['id']}, $row->{$this->columns['role']}, $row);
            else
                $user = new \Nette\Security\Identity($row->{$this->columns['id']}, null, $row);
            
            return $user;
            
	}
        
        /**
         * Adds a new user to the database with the given data.
         * 
         * @param array $data
         * @param array $columns Optional filter only certain columns from the data array.
         */
        public function add($values, $columns = null){
            
            // if requested filter the input data
            if ($columns)
                $this->filterValues($values, $columns);
            
            // check if the provided data is complete and valid
            if (!$this->validateUserData($values))
                throw new \Nette\Security\AuthenticationException("Invalid data.", self::INVALID_DATA);
            
            // check if the provided username is available
            if ($this->exists($values[$this->columns['username']]))
                throw new \Nette\Security\AuthenticationException("Username is already taken.", self::USERNAME_EXISTS);
            
            // salt the password, if it is provided
            if (isset($values[$this->columns['password']]))
                $values[$this->columns['password']] = $this->calculateHash ($values[$this->columns['password']]);
            
            // insert the user to the DB
            parent::add($values);
            
        }
        
        /**
         * Modifies data for a user found by a given column.
         * 
         * @param string $column
         * @param mixed $value
         * @param array $values
         * @param array $columns
         */
        public function modifyBy($column, $value, $values, $columns = null) {
            
            // salt the password, if it is provided
            if (isset($values[$this->columns['password']]))
                $values[$this->columns['password']] = $this->calculateHash ($values[$this->columns['password']]);
            
            parent::modifyBy($column, $value, $values, $columns);
            
        }
        
        /**
         * Checks if the given username is already registered.
         * 
         * @param string $username
         */
        public function exists($username){
            
            // check the DB
            $row = $this->getBy($this->columns['username'], $username)->fetch();
            
            return (boolean) $row;
            
        }

        
        /**
         * Validates the provided data to contain all the necessary user information.
         * 
         * @param array $data
         */
        protected function validateUserData(array $data) {
            
            if (!isset($data, $this->columns['username']))
                return false;
            
            return true;
            
        }

        /**
         * Resolves the credentials array and looks for the given user in the DB.
         * 
         * @param array $credentials
         * @return \Nette\Database\Table\ActiveRow
         * @throws \Nette\Security\AuthenticationException
         */
        protected function getByCredentials(array $credentials) {
            
            switch (count($credentials)){
                // 2 credentials, assumes username and password
                case 2:
                    // expand the credentials
                    $username = $credentials[self::USERNAME];
                    $password = $credentials[self::PASSWORD];
                    // check the DB
                    $row = $this->getBy($this->columns['username'], $username)->fetch();
                    // compare passwords
                    if ($row->{$this->columns['password']} !== $this->calculateHash($password))
                        throw new \Nette\Security\AuthenticationException("Invalid password.", self::INVALID_CREDENTIAL);
                    break;
                // 1 credential, assume Facebook ID
                case 1:
                    // expand the credentials
                    list($fbid) = $credentials;
                    // check the DB
                    $row = $this->getBy($this->columns['fbid'], $fbid)->fetch();
                    break;
                default:
                    $row = null;
                    break;
            }
            
            return $row;
            
        }

        /**
	 * Computes salted password hash.
	 * @param  string
	 * @return string
	 */
	protected function calculateHash($password){
            
            return md5($password . str_repeat($this->salt, 7));
            
	}

}

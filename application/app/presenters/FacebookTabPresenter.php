<?php

/**
 * Homepage presenter.
 *
 * @author     Pavol Eichler
 */
class FacebookTabPresenter extends BasePresenter
{
 
    /**
     * @persistent
     */
    public $liked;
    
    /**
     * @persistent
     */
    public $lang;
    
    protected function startup() {
        parent::startup();
        
        /*
         * P3P Policy quick reference
         * Website provides access to identified data:
         *  NOI - no identified data stored
         *  ALL - access to all identified data
         *  CAO - access to onlne and physical contact data and more
         *  IDC - access to contact data only
         *  OTI - acces to other identified data
         *  NON - no access to identified data
         * Purpose of the data processing:
         *  CUR - related to the current activity the data was provided for
         *  ADM - web site or system administration
         *  DEV - research and development
         *  TAI - data used during the current session only
         *  PSA - data is not assigned to users name or contact and used for analysis
         *  PSD - data is not assigned to users name or contact and used to make decisions directly affecting the user (providing tailored content)
         *  IVA - identified user is assigned to the data used for analysis
         *  IVD - identified user is assigned to the data used  to make decisions directly affecting the user (providing tailored content)
         *  CON - contacting visitors for marketing of services or products
         *  HIS - historical preservation of data as governed by laws or policies
         *  TEL - contacting visitors for marketing of services or products by phone
         *  OTP - other uses
         * Categories of data processed:
         *  PHY - physical contact information
         *  ONL - online contact information
         *  UNI - unique identifier
         *  PUR - purchase information
         *  FIN - financial information
         *  COM - computer information
         *  NAV - navigation and click-stream data
         *  INT - interactive data
         *  DEM - demographic and socioeconomic data
         *  CNT - contents of communication (body of emails, chat..)
         *  STA - mechanisms for maintaing a stateful session (cookies..)
         *  POL - data about political or religious views
         *  HEA - health related informations
         *  PRE - informations about individual's likes or dislikes
         *  LOC - current physical location data
         *  GOV - governement issued identifiers (passport ID, NIN...)
         *  OTC - other
         * Retention time of identified data:
         *  NOR - brief time during a single interaction
         *  STP - time required by the stated purpose only
         *  LEG - time required by law for the stated purpose
         *  BUS - time determined by the providers internal business practice
         *  IND - indefiniltely
         * Recipient of data:
         *  OUR - ourselves ot entities for whom we are acting as agents
         *  DEL - delivery services which could use the data for other purposes
         *  SAM - entities that follow the same policy as ourselves
         *  UNR - entities following different practices
         *  PUB - publicly available
         *  OTR - other recipients
         */
        header('P3P: CP="OTI CUR ADM DEV ONL UNI COM NAV INT STA OUR IND"');
                
        // meta data
        $this->template->title = $this->context->translator->translate('');
        
        $this->template->meta = array(
            'og' => array(
                'type' => 'website',
                'title' => $this->context->translator->translate(''),
                'description' => $this->context->translator->translate(''),
                'image' => $this->template->baseUrl . '/i/',
                'url' => $this->link('//default')
            ),
            'fb' => array(
                'app_id' => $this->context->params['facebook']['appId']
            )
        );
        
        // facebook set up
        $this->template->facebook = array(
            'appId' => $this->context->params['facebook']['appId'],
            'locale' => $this->context->params['facebook']['locale']
        );
        
        // analytics set up
        $this->template->analytics = array(
            'id' => $this->context->params['analytics']['id']
        );
        
        // try to recognize the user
        $this->registerUser();
        
        // check like status
        $signedRequest = $this->context->facebook->getSignedRequest();
        if ($signedRequest AND isset($signedRequest['page'])){
            // set persistent parameter
            $this->liked = $signedRequest['page']['liked'];
        }
    }

    /**
     * Shows the Facebook app canvas page.
     */
    public function renderCanvas() {
        
        // Redirect the user back to the application tab on the client's Facebook page.
        $this->setView('redirect');
        
        $this->template->url = 'http://www.facebook.com/pages/page/' . $this->context->params['facebook']['pageId'] . '/?sk=app_' . $this->context->params['facebook']['appId'];
        
    }
    
    /**
     * Updates the list of user's Facebook friends in the database.
     * Requires a friends database service registered in context.
     */
    public function actionUpdateFriends() {
        
        if (!$fbid = $this->context->facebook->getUser())
            $this->terminate();
        
        // fetch all friends of the current user from Facebook
        $friends = array();
        try {
            $limit = 2000;
            $offset = 0;
            // call the API in a loop to fetch them all (Facebook will not serve a complete list of friends in one call for really large friend lists)
            do {
                $response = $this->context->facebook->api('/me/friends', 'GET', array('limit' => $limit, 'offset' => $offset));
                $friends = array_merge($friends, $response['data']);
                $offset += $limit;
            }while(count($response['data']));
        }  catch (Exception $exc){
            // unknown error
        }
        
        // prepare the rows for insert
        array_walk($friends, function(&$row) use ($fbid) {
            $row = array(
                'fbid' => $row['id'],
                'user' => $fbid,
                'name' => $row['name']
            );
        });
        
        $this->context->friends->deleteByUser($fbid);
        $this->context->friends->add($friends);
        
        $this->terminate();
        
    }
    
    /**
     * Checks if the Facebook user has granted permissions to access his account and logs she in / registers her.
     */
    protected function registerUser() {
        
        // get the user ID
        $fbid = $this->context->facebook->getUser();
        
        try {
            // we know the ID
            // if this is a new user try to log in
            $info = $this->user->isLoggedIn() ? $this->user->getIdentity()->getData() : null;
            if (!$info OR $info['fbid'] != $fbid)
                $this->user->login($fbid);
        } catch (\Nette\Security\AuthenticationException $exc) {
            // login failed
            if ($exc->getCode() == Nette\Security\IAuthenticator::IDENTITY_NOT_FOUND){
                // this is a new user
                try {
                    // get her complete info
                    $info = $this->context->facebook->api('/me');
                    // create an arary of data to save to the DB
                    $save = array(
                        'fbid' => $info['id'],
                        'name' => $info['name'],
                        'first_name' => $info['first_name'],
                        'last_name' => $info['last_name'],
                        'created' => new DateTime()
                    );
                    // we may not have the user's email
                    if (isset($info['email']))
                        $save['email'] = $info['email'];
                    // add her to the database
                    $this->context->users->add($save);
                    // now, log her in
                    $this->user->login($info['id']);
                }catch(FacebookApiException $fbexc){}
            }
        }
        
    }

    /**
     * Requires the user to like the client's page. If the user did not like the page, she is served by the unliked action.
     */
    protected function requireLike() {
        
        // require user to like the app
        if (!$this->liked)
            $this->forward ('unliked');
        
    }
    
}
<?php

namespace Models;

use Nette\Security as NS;

/**
 * Users authenticator.
 */
class Authenticator extends \Nette\Object implements NS\IAuthenticator
{
	/** @var \Nette\Database\Table\Selection */
	protected $table;
        /** @var string */
        protected $salt = null;
        /** @var array */
        protected $columns = array(
            'id' => 'id',
            'username' => 'username',
            'password' => 'password',
            'role' => 'role'
        );

        public function __construct($salt, \Nette\Database\Table\Selection $table, $columns = null){
            
            $this->table = $table;
            $this->salt = $salt;
            
            // overwrite column names, if requested
            if ($columns)
                $this->columns = array_merge($this->columns, $columns);
            
	}

        /**
	 * Performs an authentication
	 * @param  array
	 * @return Nette\Security\Identity
	 * @throws Nette\Security\AuthenticationException
	 */
	public function authenticate(array $credentials)
	{

            // explode variables from array
            list($username, $password) = $credentials;

            // get the array
            $row = $this->table->where($this->columns['username'], $username)->fetch();

            // unknown user
            if (!$row)
                throw new NS\AuthenticationException("User '$username' not found.", self::IDENTITY_NOT_FOUND);

            // invalid password
            if ($row->{$this->columns['password']} !== $this->calculateHash($password))
                throw new NS\AuthenticationException("Invalid password.", self::INVALID_CREDENTIAL);

            // create the user identity
            unset($row->{$this->columns['password']});
            
            // create the user
            if ($this->columns['role'])
                $user = new NS\Identity($row->{$this->columns['id']}, $row->{$this->columns['role']}, $row);
            else
                $user = new NS\Identity($row->{$this->columns['id']}, null, $row);
            
            return $user;
            
	}

	/**
	 * Computes salted password hash.
	 * @param  string
	 * @return string
	 */
	public function calculateHash($password)
	{
            return md5($password . str_repeat($this->salt, 7));
	}

}

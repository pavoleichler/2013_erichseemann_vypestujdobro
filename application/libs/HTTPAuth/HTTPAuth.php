<?php

/**
 * HTTP autentifikacia.
 * Umoznuje zadefinovant pri vytvoreni objektu ake budu mena+hesla pre prihlasenie.
 * Potom metoda protect() skontroluje ci je pouzivatel autentifikovany
 * 
 * $auth = new HTTPAuth(array(
 *     'meno'  => 'heslo'
 * ));
 * $auth->protect();
 * 
 */
class HTTPAuth
{
	/**
	 * Pole s povolenymi prihlasovacimi udajmi. meno => heslo
	 * @var array
	 */
	protected $users = array();
        protected $messages = array(
            'realm' => 'Protected area',
            'error' => 'Authentication failed'
        );
        
	/**
	 * Konstruktor. Inicializuje prihlasovacie udaje.
	 *
	 * @param array $users
	 */
	public function __construct($users = array(), $messages = null)
	{
		$this->users = $users ? $users : array();
                
                // extend the messages array
                if ($messages){
                    array_walk($this->messages, function($value, $key) use ($messages){
                        return isset($messages[$key]) ? $messages[$key] : $value;
                    });
                }
	}

	/**
	 * Kontroluje ci aktualny pouzivatel je autentifikovany.
	 */
	public function protect()
	{
		// Status flags:
		$LoginSuccessful    = false;
		$Logout             = false;
	
		// Check username and password:
		if (isset($_SERVER['PHP_AUTH_USER']) && isset($_SERVER['PHP_AUTH_PW'])){
		 
		    $usr = $_SERVER['PHP_AUTH_USER'];
		    $pwd = $_SERVER['PHP_AUTH_PW'];
		 
		    // Does the user want to login or logout?
			foreach ($this->users as $name => $pass)
			{
				if ($usr == $name && $pwd == $pass)
				{
					$LoginSuccessful = true;
					break;
				}
			}
			
			if ($usr == 'reset' && $pwd == 'reset' && isset($_GET['Logout'])){ 
		        // reset is a special login for logout ;-)
		        $Logout = true;
		    }
		}
		if ($Logout){
 
		    // The user clicked on "Logout"
		    print 'You are now logged out.';
			die();
		}
		else if ($LoginSuccessful){
		 
		    // The user entered the correct login data, put
		    // your confidential data in here: 
		    //print 'You reached the secret page!<br/>';
		    //print '<br/>';
		 
		    // This will not clear the authentication cache, but
		    // it will replace the "real" login data with bogus data
		    //print '<a href="http://reset:reset@'. $CurrentUrl .'?Logout=1">Logout</a>';
		}
		else {
		 
		    /* 
		    ** The user gets here if:
		    ** 
		    ** 1. The user entered incorrect login data (three times)
		    **     --> User will see the error message from below
		    **
		    ** 2. Or the user requested the page for the first time
		    **     --> Then the 401 headers apply and the "login box" will
		    **         be shown
		    */
		 
		    // The text inside the realm section will be visible for the 
		    // user in the login box
		    header('WWW-Authenticate: Basic realm="'.$this->messages['realm'].'"');
		    header('HTTP/1.0 401 Unauthorized');
		 
		    // Error message
		    print $this->messages['error'];
			die();
		}
		
		// setko OK, moze ist dalej
	}
}

?>
#
# SQL to perform the initial setup for the application's DB.
# Example:
#
# CREATE TABLE `table` (
#   `id` INT(4) UNSIGNED AUTO_INCREMENT NOT NULL,
#   `parent_id` INT(4) UNSIGNED NOT NULL,
#   `index` INT(4) UNSIGNED NOT NULL,
#   PRIMARY KEY (`id`),
#   FOREIGN KEY (`parent_id`) REFERENCES PARENT(`id`),
#   INDEX(`index`)
# ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_slovak_ci;
# 